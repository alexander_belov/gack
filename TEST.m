T = 1.5^(0)*0.172; % 0 -> 2000 K, 1 -> 3000 K, 2 -> 4500 K, 3 -> 6750 K
int_span = 1e-5; % integration interval
epsilon_user = 1e-2; % required accuracy
solver_name = 'time_2';

[u, epsilon] = GACK(T, int_span, epsilon_user, solver_name);
epsilon % display actual accuracy