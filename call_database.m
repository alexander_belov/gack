function call_database
% Reads database from "Reactions.txt" file. In order to extend the database
% and account for new components it's necessary to add the titles of the
% components into the "C" structure.

global components_num;
global C;
C = cellstr( char('O2','H2','O','H','OH','HO2','H2O2','H2O','O3') );
components_num = length(C);

global S;
fid = fopen('Reactions.txt','r');
S0 = fscanf(fid,'%g');
max_R = length(S0)/9;
S = zeros(max_R,9);
for r = 1:max_R
    for j = 1:9
        S(r,j) = S0(j + 9*(r-1));
    end
end
end