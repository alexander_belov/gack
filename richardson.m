function R0 = richardson( u1, u2, solver_name )
% Calculates point-wise estimation according to Richardson method.

dim = size(u1);
R0 = zeros(dim(1),dim(2));

if ( strcmp(solver_name,'time_1') == 1 || strcmp(solver_name,'arc_length_1') == 1 )
    p = 1;
elseif ( strcmp(solver_name,'time_2') == 1 || strcmp(solver_name,'arc_length_2') == 1 )
    p = 2;
end

for j = 1:dim(1)
    for n = 1:dim(2)
        R0(j,n) = ( u2( j,2*n-1 ) - u1( j,n ) )/( 2^p-1 );
    end
end
end