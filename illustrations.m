function illustrations( t, u, J, N0, precision_output, D_output )
% Auxiliary graphics: solution obtained on the thickest mesh and
% convergence curves for accuracy and disbalance.

global C;

figure; xlabel('Time, s'); ylabel('Concentrations, mole/cm3')
title('SOLUTION COMPONENTS');
hold on
temp = zeros(1,length(t));
C1 = cell(length(J),1);
for j = 1:length(J)
    for n = 1:length(t)
        temp(n) = u(J(j),n);
    end
    C1(j) = C( J(j) );
    plot(t,temp,'LineWidth',2);
end
legend(char(C1))

figure; xlabel('lg N'); ylabel('lg epsilon')
title('CONVERGENCE: RELATIVE ACCURACY');
hold on
N = zeros(1,length(precision_output));
for m = 1:length(precision_output)
    N(m) = N0*2^(m);
end
plot(log10(N),log10(precision_output),'-ok','MarkerEdgeColor','k','MarkerFaceColor','k','MarkerSize',11)

figure; xlabel('lg N'); ylabel('lg D')
title('CONVERGENCE: RELATIVE DISBALANCE')
hold on
N = zeros(1,length(precision_output)+1);
for m = 1:length(precision_output)+1
    N(m) = N0*2^(m-1);
end
plot(log10(N),log10(D_output),'-ok','MarkerEdgeColor','k','MarkerFaceColor','k','MarkerSize',11)
end