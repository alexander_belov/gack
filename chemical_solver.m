function [t,u] = chemical_solver( u0, int_span, N, T, J, R, solver_name )
% Solves the ODE system corresponding to chemical kinetics problem on a
% mesh with N steps. The solvers implied are the chemical schemes with 1
% and 2 iterations in "time" and "arc length" arguments.

global components_num;

step = int_span/N;
        
u = zeros(components_num, N+1);
t = zeros(1,N+1);
u(:,1) = u0;

for n = 1:N
    if ( strcmp(solver_name,'time_1') == 1 )
    
        [phi,psi] = right_hand( u(:,n), T, J, R );
        u(:,n+1) = chemical_scheme( u(:,n), J, phi, psi, step );
        t(n+1) = t(n) + step;

    elseif ( strcmp(solver_name,'time_2') == 1 )

        [phi,psi] = right_hand( u(:,n), T, J, R );
        u(:,n+1) = chemical_scheme( u(:,n), J, phi, psi, step );
        
        u_half = (u(:,n) + u(:,n+1))/2;
        [phi,psi] = right_hand( u_half, T, J, R );
        u(:,n+1) = chemical_scheme( u(:,n), J, phi, psi, step );

        t(n+1) = t(n) + step;

    elseif ( strcmp(solver_name,'arc_length_1') == 1 )

        [phi,psi] = right_hand( u(:,n), T, J, R );
        
        sum = 0;
        for j = 1:length(J)
            sum = sum + ( phi(J(j)) - u(J(j))*psi(J(j)) )^2;
        end
        phi = phi/sqrt( 1 + sum ); psi = psi/sqrt( 1 + sum );   
        u(:,n+1) = chemical_scheme( u(:,n), J, phi, psi, step );
        
        phi_t = 1/sqrt(1 + sum); psi_t = 0;
        t(n+1) = chemical_scheme( t(n), 1, phi_t, psi_t, step );

    elseif ( strcmp(solver_name,'arc_length_2') == 1 )
        
        [phi,psi] = right_hand( u(:,n), T, J, R );
        
        sum = 0;
        for j = 1:length(J)
            sum = sum + ( phi(J(j)) - u(J(j))*psi(J(j)) )^2;
        end
        phi = phi/sqrt( 1 + sum ); psi = psi/sqrt( 1 + sum );   
        u(:,n+1) = chemical_scheme( u(:,n), J, phi, psi, step );
        
        phi_t = 1/sqrt(1 + sum); psi_t = 0;
        t(n+1) = chemical_scheme( t(n), 1, phi_t, psi_t, step );
        
        u_half = (u(:,n) + u(:,n+1))/2;
        
        [phi,psi] = right_hand( u_half, T, J, R );
        
        sum = 0;
        for j = 1:length(J)
            sum = sum + ( phi(J(j)) - u_half(J(j))*psi(J(j)) )^2;
        end
        phi = phi/sqrt( 1 + sum ); psi = psi/sqrt( 1 + sum );
        u(:,n+1) = chemical_scheme( u(:,n), J, phi, psi, step );
        
        phi_t = 1/sqrt(1 + sum); psi_t = 0;
        t(n+1) = chemical_scheme( t(n), 1, phi_t, psi_t, step );      
    end
end
end