function [u, epsilon] = GACK( T, int_span, epsilon_user, solver_name )
% Solves chemical kinetics problem with guaranteed accuracy. The user sets
% temperature (in eV), integration interval and, required relative accuracy
% and the solver name (default: 2nd order scheme in "time" argument). The
% program displays the list of the reactions available in the database
% and proposes to input the numbers of required reactions. Then it
% determines the components involved into the reactions and proposes to
% input initial conditions for them. After that, the calculation on a
% sequence of nested is carried out until relative accuracy determined by
% Richardson method is less than user-required accuracy. The "adjusting"
% parameters are the number of steps N0 in the most coarse mesh and maximum
% number of nested meshes max_grid_num.

call_database;
R = reactions_input;
[J,u0] = components_input(R);

N0 = 10;
max_grid_num = 20;
precision = zeros(1,max_grid_num);
D = zeros(1,max_grid_num+1);

% The solvers are: time1 - 1st order, argument "time"; time2 - 2nd order,
% argument "time"; arc_length1 - 1st order, argument "arc length";
% arc_length2 - 2nd order, argument "arc length".
if ( nargin < 4 )
    solver_name = 'time_2';
end

precision_temp = 1;
m = 1;
while ( precision_temp > epsilon_user )
    if ( m == 1)
        N = N0;
        
        [t,u] = chemical_solver( u0, int_span, N, T, J, R, solver_name );
        u1 = u;
        t1 = t;
        D(m) = disbalance(N, u, J);
    else
        u1 = u2;
        t1 = t2;
    end
    
    N = 2*N;
    
    [t,u] = chemical_solver( u0, int_span, N, T, J, R, solver_name );
    u2 = u;
    t2 = t;
    D(m+1) = disbalance(N, u, J);
    
    precision_u = richardson( u1, u2, solver_name );
    precision_t = richardson( t1, t2, solver_name );
    for j = 1:length(J)
        for n = 1:N/2+1
            [phi,psi] = right_hand( u(:,2*n-1), T, J, R );
            precision_u(J(j),n) = precision_u(J(j),n) - precision_t(n)*( phi(J(j)) - u(J(j),2*n-1)*psi(J(j)) );
        end
    end
    norm = 0;
    for j = 1:length(J)
        norm = norm + u0(J(j));
    end
    precision_temp = max( max( abs(precision_u) ) )/norm;
    precision(m) = precision_temp;
    
    m = m+1;
    if (m > max_grid_num)
        break;
    end
end

actual_grid_num = m-1;

precision_output = zeros(1,actual_grid_num);
D_output = zeros(1,actual_grid_num+1);
for m = 1:actual_grid_num
    precision_output(m) = precision(m);
end
for m = 1:actual_grid_num+1
    D_output(m) = D(m);
end

epsilon = precision_output(actual_grid_num);

illustrations( t, u, J, N0, precision_output, D_output )
end